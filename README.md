<!-- vscode-markdown-toc -->
* 1. [Versions](#Versions)
* 2. [Requirements](#Requirements)
* 3. [Tools](#Tools)
* 4. [Project-License](#Project-License)

<!-- vscode-markdown-toc-config
	numbering=true
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

# Moved

Project is moved to https://gitlab.com/epicsoft-networks/ci






# Smarthost

A tool collection to run in the CI based on Alpine Linux


##  1. <a name='Versions'></a>Versions

Two current version `latest` of this project are always built weekly.
If you need special versions, please feel free to contact me or create a fork.

`latest` [Dockerfile](Dockerfile)


##  2. <a name='Requirements'></a>Requirements

- Docker installed - https://www.docker.com/get-started


##  3. <a name='Tools'></a>Tools

- lftp
- gettext
- aws-cli
- p7zip
- rsync
- openssh-client-default
- sshpass
- tzdata

##  4. <a name='Project-License'></a>Project-License

MIT License see [LICENSE](LICENSE)
